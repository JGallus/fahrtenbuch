package pij;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Fahrtenbuch extends Application {
    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL location = getClass().getClassLoader().getResource("login.fxml");
        System.out.println(location);
        Pane root = FXMLLoader.load(location);
        Scene start = new Scene(root);

        primaryStage.setScene(start);
        primaryStage.show();
    }
}
